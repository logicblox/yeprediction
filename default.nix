{ pkgs ? import <nixpkgs> { inherit system; }
, system ? builtins.currentSystem
, yeprediction ? ./.
, version ? "1.0.0"
, doCheck ? false
}:

with pkgs;

rec {

    API = python36Packages.buildPythonPackage {
        name = "yeprediction-API-${version}";
        src = yeprediction;
        buildInputs = [ pkgs.nginx ];
        pythonPath =
        [   pkgs.nixops
            pkgs.git
            pkgs.mercurial  ] ++
        (with pkgs.python36Packages; [
            werkzeug
            flask
            click
            jinja2
            markupsafe
            itsdangerous
            flask-jwt-extended
        ]);
        dontStrip = true;
    };

    release = pkgs.stdenv.mkDerivation {
        name = "yeprediction-release-${version}";
        src = yeprediction;
        buildInputs = [ API ];
        installPhase = ''
            mkdir -p $out/nix-support
            echo "Copying API artifacts in derivation"
            cp -R ${API}/* $out
        '';
    };
}
