import argparse
import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import pdb

def rename_columns(in_df):
    names_mapping = {
        'Incident I D': 'redundant_id',
        'Incident Created Date': 'incident_created_date',
        'Customer Name': 'customer_name',
        'Country': 'country',
        'Partner Name': 'partner_name', 
        'Product Line': 'product_line',
        'Product': 'product', 
        'Summary': 'summary',
        'Severity': 'severity',
        'Resolution Type': 'resolution_type',
        'Description': 'description', 
        'Resolution': 'resolution', 
        'Incident ID': 'incident_id',
        'Event Details': 'event_details' }
    return in_df.rename(columns = names_mapping)

def drop_extra_columns(in_df):
    extra_columns = ['redundant_id', 'product_line']
    return in_df.drop(columns = extra_columns)

def drop_na_columns(in_df):
    return in_df.dropna(axis = 'columns', how = 'all')

def drop_duplicate_rows(in_df):
    return in_df.drop_duplicates()

def keep_english_only(in_df):
    english_speaking = ['Saudi Arabia', 'Ireland', 'United Kingdom', 'Egypt', 'Switzerland', 'Philippines', 'Thailand',
        'New Zealand', 'Hong Kong', 'Qatar', 'Oman', 'Netherlands', 'Uganda', 'Italy', 'Poland', 'Bahamas', 'Russian Federation',
        'Singapore', 'Ghana', 'Canada', 'Kenya', 'Malaysia', 'Myanmar', 'Botswana', 'Serbia', 'Macau', 'Kenya',
        'Australia', 'Nigeria', 'Lebanon', 'India', 'Barbados', 'Luxembourg', 'Greece', 'Bermuda', 'Hungary', 'Côte d\'Ivoire',
        'USA', 'United Arab Emirates', 'Bahrain', 'Kuwait', 'Singapore', 'Nigeria', 'Brunei', 'Cayman Islands']
    return in_df[(in_df['country'].isin(english_speaking))]

def merge_rows_with_same_index(in_df):
    grouped_df = in_df.groupby(['incident_id'])
    return grouped_df.aggregate({ 'incident_id': lambda x: x.count(),
                      'incident_created_date': lambda x: pd.to_datetime(x.unique()),
                      'customer_name': lambda x: x.unique(),
                      'country': lambda x: x.unique(),
                      'product': lambda x: x.unique(),
                      'summary': lambda x: x.unique(),
                      'severity': lambda x: x.unique(),
                      'description': lambda x: x.unique(),
                      'resolution': lambda x: x.unique(),
                      'resolution_type': lambda x: x.unique(),
                      'event_details': lambda x: x.str.cat(sep=' --- ') }).rename(columns = {'incident_id': 'distinct_entries_count'})

def split_incident_created_date(in_df):
    in_df[['year', 'month']] = in_df['incident_created_date'].apply(lambda x: pd.Series(x.strftime("%Y,%m").split(",")))
    return in_df.drop(columns = ['incident_created_date'])

def combine_description_resolution_event_details(in_df):
    in_df['description_resolution_event_details'] = in_df['description'] + ' --- ' + in_df['event_details'] + ' --- ' + in_df['resolution']
    in_df['description_resolution_event_details'] = in_df['description_resolution_event_details'].fillna('')
    return in_df.drop(columns = ['description', 'resolution', 'event_details'])

def remove_stop_words(text):
    stop_words = set(stopwords.words('english'))
    stop_words.update(['hi', 'hello', 'greetings', 'sunsystems', 
                       'please', 'asap', 'thank', 'you', 'thanks'])
    word_tokens = word_tokenize(text)
    filtered_sentence = [w for w in word_tokens if(w.isalpha() and w not in stop_words)]
    return " ".join(filtered_sentence)

def apply_remove_stop_words_to_dataframe(in_df):
    in_df['description_resolution_event_details'] = in_df['description_resolution_event_details'].apply(remove_stop_words)
    return in_df

def to_lowercase(in_df):
    in_df['description_resolution_event_details'] = in_df['description_resolution_event_details'].str.lower()
    return in_df

def count_keyword_occurences_in_text(keyword, text):
    _text_ = ' ' + text + ' '
    return 1 if (_text_.find(' ' + keyword + ' ') != -1) else 0

def score_incident(in_df):
    keywords = ['closing_year','closing books', 'year_end', 'financial_close', 'annual_close', 'financial_end']
    
    for keyword in keywords:
        in_df[keyword] = in_df['description_resolution_event_details'].apply(lambda x: count_keyword_occurences_in_text(keyword.replace('_', ' '), x))
    
    in_df['score'] = np.zeros(len(in_df))

    for keyword in keywords:
      in_df['score'] += in_df[keyword]

    in_df['score'] = (in_df['score'] / len(keywords)) * 100
    
    return in_df

def sort_by_descending_score(in_df):
    return in_df.sort_values('score', ascending = False)

def main(in_file, out_file):
    raw_df = pd.read_csv(in_file)
    # actual data pipeline
    out_df = (raw_df.pipe(rename_columns)
                             .pipe(keep_english_only)
                             .pipe(drop_duplicate_rows)
                             .pipe(drop_na_columns)
                             .pipe(drop_extra_columns)
                             .pipe(merge_rows_with_same_index)
                             .pipe(split_incident_created_date)
                             .pipe(combine_description_resolution_event_details)
                             .pipe(to_lowercase)
                             .pipe(apply_remove_stop_words_to_dataframe)
                             .pipe(score_incident)
                             .pipe(sort_by_descending_score))
    out_df.to_csv(out_file)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Takes in incident data extracted from Xtreme and \
        runs a series of consolidation and transformation steps on it.')
    parser.add_argument("input_file_path", type=str, help="Path to the input file that needs to be cleaned")
    parser.add_argument("output_file_path", type=str, help="Path where the resulting file will be saved")
    args = parser.parse_args()
    main(args.input_file_path, args.output_file_path)