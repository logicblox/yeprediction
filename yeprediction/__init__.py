import os
from flask import Flask
from .extensions import jwt

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'yeprediction.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # setting up database
    from . import db
    db.init_app(app)

    app.config['JWT_SECRET_KEY'] = 'super-secret'  # Change this!
    jwt.init_app(app)

    # registring blueprints, each one of these corresponds to a routes container/group
    from .routes.authentication import authentication
    from .routes.api import api
    app.register_blueprint(authentication)
    app.register_blueprint(api)

    return app