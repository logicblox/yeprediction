from flask import Blueprint, jsonify
from flask_jwt_extended import (
    jwt_required,
    get_jwt_identity
)

api = Blueprint('api', __name__, url_prefix='/api')

@api.route('/get_kbs', methods=['GET'])
@jwt_required
def get_kbs():
    # MM: for now this is just returning the current user. Need to call the AWS endpoint service here
    # once Salwa is done with her testing/implementation.
    # Access the identity of the current user with get_jwt_identity
    current_user = get_jwt_identity()
    return jsonify(logged_in_as=current_user), 200