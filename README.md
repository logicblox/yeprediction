# README #

`YEPrediction` is a piece of software that uses a logistic regression model to predict the likelihoods of occurence of Year End Processes for `Sun Systems` customers. The tool produces a forecast for the future 12 months only.

### What is this repository for? ###

This repo contains the backend code that powers the prediction API as well as the frontend code used to provide end users with a basic UI to issue API requests and visualize the response in a human-readable format.

### Tools you need (one time setup) ###

Make sure you have these tools installed:

* python 3.7.x
* pip3.7
* virtualenv (`pip3.7 install --user virtualenv`)

Run the following command to setup your virtual environment:

* `./script/setup_venv.sh`

Activate your virtual environment and install the project dependencies:

* `source ./venv/bin/activate`
* `pip install -r requirements.txt`

### Typical dev workflow ###

Activate your virtual env:

* `source ./venv/bin/activate`
* `source ./script/flask_conf.sh`

Optionally run (is database schema has been updated):

* `flask init-db`

Serve up the application:

* `flask run`

Testing Existing APIs:

* Login:
        `curl -X POST --data '{"username": "usr", "password": "pwd"}' -H "Content-Type: application/json" http://localhost:5000/auth/login`
* Get KBs:
        `curl -X GET -H "Authorization: Bearer <ACCESS_TOKEN_FROM_RESPONSE_TO_LOGIN_API>" http://localhost:5000/api/get_kbs`

### Who do I talk to? ###

* [Slack channel](https://iretail.slack.com/messages/CG1SYCSUB)